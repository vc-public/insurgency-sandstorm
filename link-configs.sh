#!/bin/sh
ln -s /home/vincent/code/dev/insurgency-sandstorm/insurgency-sandstorm.service /etc/systemd/system/insurgency-sandstorm.service &&

rm -rf /home/vincent/insurgency/serverfiles/Insurgency/Config/Server &&
ln -s /home/vincent/code/dev/insurgency-sandstorm/game-config/ /home/vincent/insurgency/serverfiles/Insurgency/Config/Server &&

sudo rm -f /home/vincent/insurgency/serverfiles/Insurgency/Saved/Config/LinuxServer/Game.ini && 
ln -s /home/vincent/code/dev/insurgency-sandstorm/Game.ini /home/vincent/insurgency/serverfiles/Insurgency/Saved/Config/LinuxServer/Game.ini &&

rm -rf /home/vincent/insurgency/lgsm/config-lgsm/inssserver &&
ln -s /home/vincent/code/dev/insurgency-sandstorm/server-config/ /home/vincent/insurgency/lgsm/config-lgsm/inssserver